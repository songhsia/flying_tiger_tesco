onload = function(){
	var myC = document.getElementById('myCount');
	var b = document.getElementsByTagName('b')[0];
	var myCount_ul = document.getElementById('myCount_ul');
	myC.onmouseover = function(){
		b.style.backgroundImage = "url(images/red.jpg)";
	}
	myC.onmouseout = function(){
		b.style.backgroundImage = "url(images/sitenav.png)";
	}
	myCount_ul.onmouseover = function(){
		myC.style.background = "#c7012c";
		myC.style.color = "#fff";
		b.style.backgroundImage = "url(images/red.jpg)";
		myCount_ul.style.display = "block";
	}
	myCount_ul.onmouseout = function(){
		myC.style.background = "";
		myC.style.color = "#666";
		b.style.backgroundImage = "url(images/sitenav.png)";
		myCount_ul.style.display = "none";
	}
/*--------验证码中的随机数字------*/
var randomnum = document.getElementsByClassName("randomnum")[0];
var changepic = document.getElementsByClassName("changepic")[0];
randomnum.innerText = Math.round(Math.random()*10000);
changepic.onclick = function(){
	randomnum.innerText = Math.round(Math.random()*10000);
}
/*--------注册验证--------*/
	var t1 = document.getElementById("tips_1");
	var t2 = document.getElementById("tips_2");
	var t3 = document.getElementById("tips_3");
	var t4 = document.getElementById("tips_4");
	var t5 = document.getElementById("tips_5");
	var t6 = document.getElementById("tips_6");
	var t7 = document.getElementById("tips_7");
	var f1 = document.getElementsByClassName("foc_1")[0];
	var f2 = document.getElementsByClassName("foc_2")[0];
	var f3 = document.getElementsByClassName("foc_3")[0];
	var f4 = document.getElementsByClassName("foc_4")[0];
	var f5 = document.getElementsByClassName("foc_5")[0];
	var f6 = document.getElementsByClassName("foc_6")[0];
	var f7 = document.getElementsByClassName("foc_7")[0];
	/*用户名验证*/
	f1.onfocus = function(){
		t1.style.display = "block";
	}
	f1.onblur = function(){
		if(f1.value==""){
			t1.style.display = "block";
			t1.innerText="用户名不能为空";
		}else{
			t1.innerText="";
			if((/^[a-zA-Z0-9]+$/).test(f1.value)){
				t1.style.display = "none";
			}else{
				t1.style.display = "block";
				t1.innerText="用户名必须由字母和数字组成";
			}
		}
	}
	/*密码验证*/
	f2.onfocus = function(){
		t2.style.display = "block";
	}
	f2.onblur = function(){
		if(f2.value==""){
			t2.style.display = "block";
			t2.innerText="密码不能为空";
		}else{
			t2.innerText="";
				if((/(?=.*[0-9])(?=.*[a-z])(?=.*[!@#$%^&*])(?=.*[A-Z]).{6,16}/) .test(f2.value)){
				t2.style.display = "none";
			}else if(/^[0-9]+$/.test(f2.value)){
				t2.style.display = "block";
				t2.innerText="密码不能全为数字，请使用字母、数字、符号至少2种的组合";
			}else if(/^[a-zA-Z]+$/.test(f2.value)){
				t2.style.display = "block";
				t2.innerText="密码不能全为字母，请使用字母、数字、符号至少2种的组合";
			}
			else{
				t2.style.display = "none";
			//	t2.style.display = "block";
			//	t2.innerText="密码长度必须在6-16位之间,由英文、数字及特殊字符组成";
				}
			}
		}
	/*确认密码验证*/
	f3.onfocus = function(){
		t3.style.display = "block";
	}
	f3.onblur = function(){
		if(f2.value==""){
			t3.style.display = "block";
			t3.innerText="确认密码不能为空";
		}else if (f3.value==f2.value) {
			t3.style.display = "none";
		}else{
			t3.style.display = "block";
			t3.innerText="两次输入的密码不一致";
		}
	}
	/*邮箱验证*/
	f4.onfocus = function(){
		t4.style.display = "block";
	}
	f4.onblur = function(){
		if(f4.value==""){
			t4.style.display = "block";
			t4.innerText="邮箱不能为空";
		}else{
			if((/^\w+@\w+\.(com|cn)/).test(f4.value)){
				t4.style.display = "none";
			}else{
				t4.style.display = "block";
				t4.innerText="邮箱格式不正确";
			}
		}
	}
	/*推荐人用户名验证*/
	f5.onfocus = function(){
		t5.style.display = "block";
	}
	f5.onblur = function(){
		t5.style.display = "none";
	}
	/*活动代码验证*/
	f6.onfocus = function(){
		t6.style.display = "block";
	}
	f6.onblur = function(){
		t6.style.display = "none";
	}
	/*验证码验证*/
	f7.onfocus = function(){
		t7.style.display = "block";
	}
	f7.onblur = function(){
		if(f7.value==randomnum.innerText){
			t7.style.display = "none";
		}else{
			t7.style.display = "block";
			t7.innerText="验证码不正确";
		}
	}
/*------cookie函数的封装--------*/
function setCookie(_name, _value, _date){
	var d=new Date();
	d.setDate(d.getDate()+_date);
	document.cookie=_name+"="+encodeURIComponent(_value)+"; path=/; expires="+d.toGMTString();	
}
function getCookie(_name){
	var str=document.cookie;
	var arr=str.split("; ");
	for(var i=0; i<arr.length; i++){
		var col=arr[i].split("=");	
		if(col[0]==_name){
			return decodeURIComponent(col[1]);
		}
	}
	return "";
}
/*--------登录--------*/
var regbtn = document.getElementsByClassName("regbtn")[0];
var thecheck = document.getElementsByClassName("thecheck")[0];
regbtn.onclick = function(){
	if (thecheck.checked) {
		alert("恭喜您注册成功");
		setCookie("username", f1.value, 100);
		setCookie("password", f2.value, 100);
		setCookie("userlogin", f1.value, 100);
		location.href="index.html";
	}else{
		alert("要先阅读并同意飞虎乐购用户协议哦！");
	}
}





}
	
	
	
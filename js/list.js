onload = function(){
	//--------top层账户与导航的下拉--------
	var myCount = document.getElementById("myCount");
	var degimg_0 = document.getElementById("degimg_0");
	var myCountul_0 = document.getElementById("myCountul_0");

	var netNav = document.getElementById("netNav");
	var degimg_1 = document.getElementById("degimg_1");
	var netNavul_0 = document.getElementById("netNavul_0");

	myCount.onmouseover = function(){
		degimg_0.style.transform = "rotate(180deg)";
		myCountul_0.style.display = "block";
		myCountul_0.style.border = "1px solid #ccc";
		myCountul_0.style.borderTop = "none";
	}
	myCount.onmouseout = function(){
		degimg_0.style.transform = "";
		myCountul_0.style.display = "none";
	}

	netNav.onmouseover = function(){
		degimg_1.style.transform = "rotate(180deg)";
		netNavul_0.style.display = "block";
		netNavul_0.style.border = "1px solid #ccc";
		netNavul_0.style.borderTop = "none";
	}
	netNav.onmouseout = function(){
		degimg_1.style.transform = "";
		netNavul_0.style.display = "none";
	}
//-----------购物车-----------
	var shopping = document.getElementsByClassName("shopping")[0];
	var cargoods = document.getElementsByClassName("cargoods")[0];
	var balance = document.getElementsByClassName("balance")[0];
	var timer1 = null;
	var isin = false;
	shopping.onmouseover = function(){
		cargoods.style.display = "block";
		clearTimeout(timer1);
	}
	shopping.onmouseout = function(){
		clearTimeout(timer1);
		timer1 = setTimeout(function(){
			if(!isin){
				cargoods.style.display = "none";
			}
		},1000);
	}
	cargoods.onmouseover = function(){
		isin = true;
	}
	cargoods.onmouseout = function(){
		isin = false;
		this.style.display = "none";
	}
	balance.onclick = function(){
		window.location="cart.html";
	}
	//--------搜索框-----------
var txt = document.getElementsByClassName("txt")[0];
var btn = document.getElementsByClassName("btn")[0];
var txtxl = document.getElementsByClassName("txtxl")[0];
var list= txtxl.children;
	txt.onclick = function(e){
	var evt = e || event;
	txtxl.style.display = "block";
		for(var i in list){
		list[i].index = i;
		list[i].onclick = function(){
		txt.value = list[this.index].innerText;
		txtxl.style.display = "none";
		}
	}
}

//---------获取首页商品json数据--------
/*	var url = "data/list.json";
	ajax.get(url, function(str){
	var obj = eval("("+str+")");
	arr = obj.list;
	var arrthedl = document.getElementsByClassName('goodsdl');
		for(var i=0;i<arr.length/2;i++){
			var ob = arr[i];
			var obl = arrthedl[i];
			var thedt = document.createElement("dt");
			var thea = document.createElement("a");
			var img = document.createElement("img");
			img.src = ob.src;
			thea.appendChild(img);
			thedt.appendChild(thea);
			thea.href = ob.href;
			var thedd = document.createElement("dd");
			var thep = document.createElement("p");
			thep.innerHTML = ob.p;
			thedd.appendChild(thep);
			var thediv = document.getElementsByClassName("pricecart")[i];
			var thea1 = document.createElement("a");		
			var thesp = document.createElement("span");
			thesp.innerHTML = "￥"+ob.span;
			thea1.appendChild(thesp);	
			thediv.appendChild(thea1);
			thea1.href = ob.href;
			thedd.appendChild(thediv);
			arrthedl[i].appendChild(thedt);
			arrthedl[i].appendChild(thedd);
		}
	});		*/
//-----------分页--------------
/*var url = "data/list.json";
	ajax.get(url, function(str){
	var obj = eval("("+str+")");
	arr = obj.list;
	var goodsright = document.getElementsByClassName('goodsright')[0];
		for(var i=0;i<arr.length/2;i++){
			var ob = arr[i]; 
			var thedl = document.createElement("dl");
			thedl.style.width = "197px";
			thedl.style.height = "241px";
			thedl.style.borderRight = "1px solid #e5e5e5";
			thedl.style.marginTop = "15px";
			thedl.style.float = "left";
			var thedt = document.createElement("dt");
			var thea = document.createElement("a");
			var img = document.createElement("img");
			img.style.width = "150px";
			img.style.height = "150px";
			img.style.paddingLeft = "17px";
			img.src = ob.src;
			thea.appendChild(img);
			thedt.appendChild(thea);
			thea.href = ob.href;
			var thedd = document.createElement("dd");
			var thep = document.createElement("p");
			thep.style.width = "150px";
			thep.style.paddingLeft = "17px";
			thep.style.fontWeight  ="600";
			thep.style.fontSize = "12px";
			thep.style.paddingTop = "8px";
			thep.innerHTML = ob.p;
			thedd.appendChild(thep);
			var pricecart = document.createElement("div");
			pricecart.setAttribute("className","pricecart");
			pricecart.style.width = "180px";
			pricecart.style.height = "42px";
			pricecart.style.background = "url('images/carticon.png') no-repeat right center";
			pricecart.style.marginTop = "10px";
			var thea1 = document.createElement("a");		
			var thesp = document.createElement("span");
			thesp.style.display = "block";
			thesp.style.fontSize = "20px";
			thesp.style.padding="10px 0 0 20px";
			thesp.style.color="#e30303";
			thesp.style.fontWeight = "600";
			thesp.innerHTML = "￥"+ob.span;
			thea1.appendChild(thesp);	
			pricecart.appendChild(thea1);
			thea1.href = ob.href;
			thedd.appendChild(pricecart);
			thedl.appendChild(thedt);
			thedl.appendChild(thedd);
			goodsright.appendChild(thedl);
		}
	});		*/
//-----------分页---------
var url = "data/data.json";
	ajax.get(url, function(str){
	var obj = eval("("+str+")");
	arr = obj.list;
	var num=10;//定义每页显示多少条数据
	var count=arr.length;//计算共有多少条数据
	var maxpage=Math.ceil(count/num);//计算共有多少页
	var page=1;	//当前显示第几页
	var goodsright = document.getElementsByClassName("goodsright")[0];
	var ol = document.getElementsByClassName("pagebtn")[0];
	function fnPage(){
		goodsright.innerHTML = "";
		ol.innerHTML = "";
		var min = (page-1)*num;
		var max = min+num-1;
		///
		for(var i=0;i<arr.length;i++){
			if(i>=min && i<=max){
			var ob = arr[i]; 
			var thedl = document.createElement("dl");
			thedl.style.width = "197px";
			thedl.style.height = "241px";
			thedl.style.borderRight = "1px solid #e5e5e5";
			thedl.style.marginTop = "15px";
			thedl.style.float = "left";
			var thedt = document.createElement("dt");
			
			var thea = document.createElement("a");
			var img = document.createElement("img");
			img.style.width = "150px";
			img.style.height = "150px";
			img.style.paddingLeft = "17px";
			img.src = ob.src;
			thea.appendChild(img);
			thedt.appendChild(thea);
			thea.href = ob.href;
			var thedd = document.createElement("dd");
			var thep = document.createElement("p");
			thep.style.width = "150px";
			thep.style.paddingLeft = "17px";
			thep.style.fontWeight  ="600";
			thep.style.fontSize = "12px";
			thep.style.paddingTop = "8px";
			thep.innerHTML = ob.p;
			thedd.appendChild(thep);
			var pricecart = document.createElement("div");
			pricecart.setAttribute("className","pricecart");
			pricecart.style.width = "180px";
			pricecart.style.height = "42px";
			pricecart.style.background = "url('images/carticon.png') no-repeat right center";
			pricecart.style.marginTop = "10px";
			var thea1 = document.createElement("a");		
			var thesp = document.createElement("span");
			thesp.style.display = "block";
			thesp.style.fontSize = "20px";
			thesp.style.padding="10px 0 0 20px";
			thesp.style.color="#e30303";
			thesp.style.fontWeight = "600";
			thesp.innerHTML = "￥"+ob.span;
			thea1.appendChild(thesp);	
			pricecart.appendChild(thea1);
			thea1.href = ob.href;
			thedd.appendChild(pricecart);
			thedl.appendChild(thedt);
			thedl.appendChild(thedd);
			goodsright.appendChild(thedl);
			}
		}
		//显示页码
		var tmp=1;//(必须为基数)表示超过该数时，显示省略号
		var tmp2=(tmp-1)/2;//用该数补全页码	
		//取显示页码的最小值及最大值
		if(page<=tmp2+1){
				min=1;
				max=tmp;
			}else if(page>=maxpage-tmp2){
				min=maxpage-tmp2*2;
				max=maxpage;
			}else{
				min=page-tmp2;
				max=page+tmp2;
			}
		//上一页
		if(page!=1){
			var li = document.createElement("li");
			li.innerHTML = "上一页";
			li.style.float = "left";
			ol.appendChild(li);
			li.onclick = function(){
				page--;
				if(page==0)page=1;
				fnPage();
			}
		}
		//显示第一页
		if(min>1){
			var li = document.createElement("li");
			li.setAttribute("page","1");
			li.innerHTML = "1";
			li.style.float = "left";
			ol.appendChild(li);
			li.onclick = function(){
				page=Number(this.getAttribute("page"));
				fnPage();
			}
		}
		//显示页码
		for(var i=2; i<=2; i++){
			var li =document.createElement("li");
			li.setAttribute("page",i);
			li.innerHTML = "...";
			li.style.float = "left";
			ol.appendChild(li);
			li.onclick = function(){
				page=Number(this.getAttribute("page"));
				fnPage();
			}
		}
		// 显示最后一页
		if(max<maxpage){
			var li =document.createElement("li");
			li.setAttribute("page",maxpage);
			li.innerHTML = maxpage;
			li.style.float = "left";
			ol.appendChild(li);
			li.onclick = function(){
				page=Number(this.getAttribute("page"));
				fnPage();
			}
		}
		//下一页
		if(page!=maxpage){//如果当前页不是最后一页
			var li = document.createElement("li");
			li.innerHTML = "下一页";
			li.style.float = "left";
			ol.appendChild(li);
			li.onclick = function(){
				page++;
				if(page>=maxpage)page=maxpage;//如果此时页数大于等于最大页数，则页数为最大页数
				fnPage();//执行函数
			}
		}
	}
	fnPage();
});
//-------标题收缩---------
$(function(){
			var flex = $(".flex");
			$(".abouts").css("background-image","url(../images/minus.png)");
			$(".abouts").click(function(e){
				if (this==e.target) {
					if ($(flex).is(":hidden")) {
						$(this).css("background-image","url(../images/minus.png");
						$(flex).show(500);
					}else{
						$(this).css("background-image","url(../images/plus.png");
						$(flex).hide(500);
					}
				}
			})
			
			var flex1 = $(".flex1");
			$(".abouts1").css("background-image","url(../images/minus.png)");
			$(".abouts1").click(function(e){
				if (this==e.target) {
					if ($(flex1).is(":hidden")) {
						$(this).css("background-image","url(../images/minus.png");
						$(flex1).show(500);
					}else{
						$(this).css("background-image","url(../images/plus.png");
						$(flex1).hide(500);
					}
				}
			});

//----------------分页---------------



		});



//------------购物车数量------------
var goodsnumb = document.getElementById("goodsnumb");
var cargoods = document.getElementsByClassName("cargoods")[0];
if(getCookie("length")!=""){
	goodsnumb.innerHTML = getCookie("length");
	cargoods.innerHTML = "您的购物车里有"+getCookie("length")+"件商品";
}else{
	goodsnumb.innerHTML = 0;
	cargoods.innerHTML = "您的购物车里还没有商品哦";
}
//---------登陆成功后用户名的修改-----------
	var welcome = document.getElementsByClassName("welcome")[0]; 
	var welto = document.getElementsByClassName("welto")[0];
	var safel = document.getElementsByClassName("safel")[0];
	var tojoin = document.getElementsByClassName("tojoin")[0];
	var fre = document.getElementsByClassName("fre")[0];
	var userlogin=getCookie("userlogin");
	if(userlogin!=""){
		welto.innerHTML = "亲爱的";
		safel.innerHTML = userlogin;
		tojoin.innerHTML = "您好";
		fre.innerHTML = "";
		var a = document.createElement("a");
		welcome.appendChild(a);
		a.innerHTML = "[退出]";
		a.onclick = function(){
			setCookie("userlogin", "", -10);
				history.go(0);
		}
	}


}

